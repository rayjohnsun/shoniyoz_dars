import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'

import './components/globalComponents'
import vuetify from './plugins/vuetify'
import 'roboto-fontface/css/roboto/roboto-fontface.css'
import '@mdi/font/css/materialdesignicons.css'
import './assets/sass/_style.scss'

import './plugins/http'

import './plugins/filters'
import './plugins/fn'

// const mixin = {
// 	data() {
// 		return {
// 			appName: 'MyAppName'
// 		}
// 	}
// }

Vue.mixin({
	data() {
		return {
			appName: 'MyAppName',
			prefixNumber: '+7'
		}
	}
})

Vue.config.productionTip = false

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')

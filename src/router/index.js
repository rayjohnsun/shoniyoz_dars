import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
// import About from '../views/About.vue'

const About1 = () => import('../views/About1.vue')
const About2 = () => import('../views/About2.vue')
const About3 = () => import('../views/About3.vue')

Vue.use(VueRouter)

const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push(location, onResolve, onReject) {
  if (onResolve || onReject) return originalPush.call(this, location, onResolve, onReject)
  return originalPush.call(this, location).catch(err => err)
}

const routes = [
  {path: '/', name: 'Home', component: Home},
  {path: '/about1', name: 'about1', component: About1},
  {path: '/about2', name: 'about2', component: About2},
  {path: '/about3', name: 'about3', component: About3},
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router

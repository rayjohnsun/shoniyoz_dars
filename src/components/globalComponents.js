import Vue from 'vue'
import Fio from './Fio'
import NameBox from './NameBox'
import FamBox from './FamBox'

Vue.component('fio', Fio)
Vue.component('name-box', NameBox)
Vue.component('fam-box', FamBox)

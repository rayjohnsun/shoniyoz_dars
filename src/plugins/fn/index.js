import Vue from 'vue'

const fn = {
	toInt(val) {
    const intVal = parseInt(val)
    if (!isNaN(intVal)) {
      return intVal
    }
    return 0
  }
}

Vue.prototype.fn = fn
export default fn

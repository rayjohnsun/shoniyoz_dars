import Vue from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
// import device from 'mobile-device-detect'
 
// const mobileId = device.browserName + '_' + device.browserVersion
// const mobileOs = device.osName + '_' + device.osVersion

axios.defaults.headers.common['Response-Format'] = 'json'
// axios.defaults.headers.common['x-mobilename'] = mobileId + '|' + mobileOs
// axios.defaults.headers.common['x-mobiledevice'] = mobileId + '|' + mobileOs
axios.defaults.headers.common['x-mobileversion'] = '2.0.0'

// if (Vue.prototype.$session.exists('login_user')) {
// 	let hexUser = Vue.prototype.$session.get('login_user')
// 	try {
// 		const user = Vue.prototype.$hex.decrypt(hexUser)
// 		if (user && 'id' in user) {
// 			axios.defaults.headers.common['x-mobileapp'] = user.id
// 		} else {
// 			axios.defaults.headers.common['x-mobileapp'] = ''
// 		}
// 	} catch {
// 		axios.defaults.headers.common['x-mobileapp'] = ''
// 	}
// } else {
// 	axios.defaults.headers.common['x-mobileapp'] = ''
// }

Vue.use(VueAxios, axios)

import Vue from 'vue'

Vue.filter('summa', (value, quote, def, append) => {
	if (!quote) { quote = ','; }
	if (!def) { def = `0${quote}00`; }
	if (value) {
		if (typeof value !== 'number') {
			value = value.toString().replace(/\s+/g, '').replace(/,/g, '.')
			value = parseFloat(value)
			if (Number.isNaN(value)) { value = 0; }
		}
		value = value.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ' ').replace(/\./g, quote)
		if (append) {
			return `${value} ${append}`
		}
		return value
	}
	if (append) {
		return `${def} ${append}`
	}
	return def
})

Vue.filter('employee', (value, append) => {
	if (!append) {
		append = 'ta'
	}
	return `${value} ${append}`
})

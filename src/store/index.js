import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
  	appVersion: 1,
  	// mashinalar: [
  	// 	{rangi: 'qizil', nomi: 'Trailblazer'},
  	// 	{rangi: 'kuk', nomi: 'Captiva'},
  	// ]
    cntAbout1: 0,
    cntAbout2: 0,
    cntAbout3: 0,
  },
  mutations: {
  	updateAppVersion(state) {
  		state.appVersion++
  	},
    updateRouteCnt(state, routeName) {
      if (routeName === 'about1') {
         state.cntAbout1++
      } else if (routeName === 'about2') {
         state.cntAbout2++
      } else if (routeName === 'about3') {
         state.cntAbout3++
      }
    }
  },
  getters: {
  	getAppVersion(state) {
  		return `${state.appVersion}.00`
  	}
  },
  actions: {
  	versiyaYagilash({state, commit}) {
  		commit('updateAppVersion')
  		state.appVersion++
  	},
  	versiyaOlish({getters}) {
  		return getters.getAppVersion
  	},
  },
})

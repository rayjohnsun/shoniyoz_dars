module.exports = {
  root: true,
  env: {
    node: true
  },
  extends: [
    'plugin:vue/essential',
    '@vue/standard'
  ],
  parserOptions: {
    parser: 'babel-eslint'
  },
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'space-before-function-paren': 'off',
    'comma-dangle': 'off',
    'no-trailing-spaces': 'off',
    'no-tabs': 'off',
    'indent': 'off',
    'object-curly-spacing': 'off',
    'no-mixed-spaces-and-tabs': 'off',
    'semi': 'off',
  }
}
